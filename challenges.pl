

add(0, X, X).
add(s(X), Y, s(Z)) :- add(X, Y, Z).

subt(X, X, 0).
subt(s(X), Y, s(Z)) :- subt(X, Y, Z).

mul(_, 0, 0).
mul(X, s(Y), Z) :- mul(X, Y, P), add(X, P, Z).

pow(_, 0, s(0)).
pow(X, s(Y), Z) :- pow(X, Y, P), mul(X, P, Z).

lenPure([], 0).
lenPure([_|T], s(L)) :- lenPure(T, L).

binDecPure([0], 0).
binDecPure([s(0)], s(0)).
binDecPure([H|T], B) :- binDecPureHelper(H, D), len(T, L), pow(s(s(0)), L, P), 
                        mul(P, D, M), add(M, D2, B), binPureDec(T, D2).
                        
binDecHelper(0, 0).
binDecHelper(s(0), s(0)).

rotLeftPure(Xs, 0, Xs).
rotLeftPure(Xs, s(N), Ys) :- shiftedList(Xs, S), rotLeftPure(S, N, Ys).

entryMatch(X, X).

entryDownNPure([X|_], 0, X).
entryDownNPure([_|Xs], s(N), Y) :- entryDownNPure(Xs, N, Y).

insertAtTail([], E, [E]).
insertAtTail([X|Xs], E, [Y|Ys]) :- entryMatch(X, Y), insertAtTail(Xs, E, Ys).

shiftedList([X|Xs], A) :- insertAtTail(Xs, X, A).

repeatPure(_, 0, []).
repeatPure([], _, []).
repeatPure([X|Xs], N, [Y|Ys]) :- matchNEntriesPure(X, [Y|Ys], N, CutList), repeatPure(Xs, N, CutList).

matchNEntriesPure(X, [X|Xs], s(0), Xs).
matchNEntriesPure(X, [Y|Ys], s(N), A) :- entryMatch(X, Y), matchNEntriesPure(X, Ys, N, A).




pureDia(L, D) :- pureFindMax(L, Max), pureFindMin(L, Min), sub(Max, Min, D).

sub(Y, Y, 0).
sub(s(X), Y, s(Z)) :- sub(X, Y, Z).

greaterThan(s(_), 0).
greaterThan(s(X), s(Y)) :- greaterThan(X, Y).

greaterThanEqual(_, 0).
greaterThanEqual(s(X), s(Y)) :- greaterThanEqual(X, Y).

lessThan(0, s(_)).
lessThan(s(X), s(Y)) :- lessThan(X, Y).

lessThanEqual(0, _).
lessThanEqual(s(X), s(Y)) :- lessThanEqual(X, Y).


pureFindMax([H|T], M) :- pureFindMaxHelper([H|T], M, H).

pureFindMaxHelper([], CurrentMax, CurrentMax).
pureFindMaxHelper([H|T], M, CurrentMax) :- greaterThan(H, CurrentMax), pureFindMaxHelper(T, M, H).
pureFindMaxHelper([H|T], M, CurrentMax) :- lessThanEqual(H, CurrentMax), pureFindMaxHelper(T, M, CurrentMax).


pureFindMin([H|T], M) :- pureFindMinHelper([H|T], M, H).

pureFindMinHelper([], CurrentMin, CurrentMin).
pureFindMinHelper([H|T], M, CurrentMin) :- lessThan(H, CurrentMin), pureFindMinHelper(T, M, H).
pureFindMinHelper([H|T], M, CurrentMin) :- greaterThanEqual(H, CurrentMin), pureFindMinHelper(T, M, CurrentMin).


% PURE (Keeps order of list)
removeAllSmaller(L, X, A) :- removeAllSmallerHelper(L, X, A, []).

removeAllSmallerHelper([], _, A, A).
removeAllSmallerHelper([H|T], X, A, Acc) :- lessThan(H, X), removeAllSmallerHelper(T, X, A, Acc).
removeAllSmallerHelper([H|T], X, A, Acc) :- lessThanEqual(X, H), append(Acc, [H], New),
                                            removeAllSmallerHelper(T, X, A, New).


dia(L, D) :- findMax(L, Max), findMin(L, Min), D is Max - Min.

findMax([H|T], M) :- findMaxHelper([H|T], M, H).

findMaxHelper([], CurrentMax, CurrentMax).
findMaxHelper([H|T], M, CurrentMax) :- H > CurrentMax, findMaxHelper(T, M, H).
findMaxHelper([H|T], M, CurrentMax) :- H =< CurrentMax, findMaxHelper(T, M, CurrentMax).


findMin([H|T], M) :- findMinHelper([H|T], M, H).

findMinHelper([], CurrentMin, CurrentMin).
findMinHelper([H|T], M, CurrentMin) :- H < CurrentMin, findMinHelper(T, M, H).
findMinHelper([H|T], M, CurrentMin) :- H >= CurrentMin, findMinHelper(T, M, CurrentMin).


poly(L, N, V) :- polyHelper(L, N, V, 0, 0).

polyHelper([], _, Accumulator, _, Accumulator).
polyHelper([H|T], N, V, Degree, Accumulator) :- Acc is Accumulator + N ^ Degree * H,
                                                Deg is Degree + 1,
                                                polyHelper(T, N, V, Deg, Acc).

len(List, L) :- lenHelper(List, L, 0).

lenHelper([], L, L).
lenHelper([_|T], L, Accumulator) :- Acc is Accumulator + 1, lenHelper(T, L, Acc).










% List functions

% LAST ELEMENT
my_last([X], X).
my_last([_|T], X) :- my_last(T, X).


% SECOND TO LAST ELEMENT
last_but_one([X|[_]], X).
last_but_one([_|T], X) :- last_but_one(T, X).


% ELEMENT AT INDEX (1 based)
element_at(1, [X|_], X).
element_at(N, [_|T], X) :- N > 1, M is N - 1, element_at(M, T, X).


% ELEMENT MATCH
elem_match(X, X).


% FIND LENGTH OF LIST
list_length(Len, L) :- list_length_helper(Len, L, 0).
list_length_helper([], L, L).
list_length_helper([_|T], X, N) :- M is N + 1, list_length_helper(T, X, M).


% REVERSE LIST
rev_list(L1,L2) :- rev_list_helper(L1,L2,[]).

rev_list_helper([],L2,L2) :- !. % ! operator ensures that we do not backtrack (eliminates multiple answers)
rev_list_helper([X|Xs],L2,Acc) :- rev_list_helper(Xs,L2,[X|Acc]).


% LIST OF ALL BUT LAST ELEMENT
all_but_last([_], []).
all_but_last([H|T], [H|Ys]) :- all_but_last(T, Ys).


% PALINDROME
is_palindrome([_]) :- !. % May not be needed
is_palindrome([X,X]) :- !.
is_palindrome([H|T]) :- rev_list(T, [H2|Middle]), entryMatch(H, H2), is_palindrome(Middle).

create_palindrome([X], [X]).
create_palindrome(L, A) :- rev_list(L, RL), appenList(L, RL, A).


% RANGE, create list with integers that are in given range
rangePure(N, N, [N]).
rangePure(M, N, L) :- range(s(M), N, K), append(L, [M], K).


% REMOVE AT ELEMENT K
remove_at(1, [_|T], T).
remove_at(K, [H|T], [H|Ys]) :- K > 1, M is K - 1, remove_at(M, T, Ys).

remove_at_2(K, L, A) :- remove_at_helper(K, L, A, []).
remove_at_helper(1, [_|T], A, Acc) :- append(Acc, T, A).
remove_at_helper(K, [H|T], A, Acc) :- K > 1, M is K - 1, append(Acc, [H], New), remove_at_helper(M, T, A, New).

% Appends a single element
appen([], X, [X]).
appen([H|T], X, [H|T2]) :- appen(T, X, T2).

% Appends an entire list
appenList(L, [], L) :- !.
appenList([X|Xs], [Y|Ys], [X|T]) :- appen(Xs, Y, New), appenList(New, Ys, T), !.

% Split at a given location in list (given location and all left goes in first list).
splitAtN(L, 0, [], L). % Handling case that we give 0. Assuming we want [] left list.
splitAtN([X|Xs], 1, [X], Xs) :- !.
splitAtN([X|Xs], N, [X|T2], RightL) :- M is N - 1, splitAtN(Xs, M, T2, RightL).


% Drop every Nth element in a list.
dropEveryNth(L, N, A) :- N > 0, dropEveryNthHelper(L, N, A, N).

dropEveryNthHelper([], _, [], _) :- !.
dropEveryNthHelper([_|T], 1, T2, N) :- dropEveryNthHelper(T, N, T2, N), !.
dropEveryNthHelper([H|T], M, [H|T2], N) :- K is M - 1, 
                                            dropEveryNthHelper(T, K, T2, N), !.

% INCLUSIVE (Both ends)
% Currently doesn't support an unknown as first argument -> e.g. slice(X, 2,4, [2,3,4])
slice([H|_], 1, 2, [H]).
slice([H|T], 1, N, [H|T2]) :- N2 is N - 1, slice(T, 1, N2, T2).
slice([_|T], M, N, T2) :- M2 is M - 1, slice(T, M2, N, T2).


% Split a list at a given element, essentially "splitAtN" above
split(L, 0, [], L) :- !.
split([H|T], 1, [H], T) :- !.
split([H|T], N, [H|T2], T3) :- M is N - 1, split(T, M, T2, T3).

% Insert an element (X) at a given location (N) in the list (L).
insertAt(X, L, 1, [X|L]).
insertAt(X, [H|T], N, [H|T2]) :- M is N - 1, insertAt(X, T, M, T2).

% BEGIN - QUIZ

between(X, Y, Z) :- lessThan(X, Y), lessThan(Y, Z).

even([]).
even([_,_|T]) :- even(T).

last([X], X).
last([_|T], X) :- last(T, X).

listSum([], 0).
listSum([H|T], Acc) :- listSum(T, NewAcc), Acc is NewAcc + H.

% NEEDED TO COVER EMPTY LIST CASE -> QUIZ MISTAKE? I added NumElems > 0 to cover this
avg(L, A) :- listSum(L, Sum), len(L, NumElems), NumElems > 0, A is Sum / NumElems.

%zip([X|_], [Y], [[X,Y]]) :- !.
%zip([X], [Y|_], [[X,Y]]).
%zip([X|Xs], [Y|Ys], [[X,Y]|T]) :- zip(Xs, Ys, T).
zip([], _, []) :- !.
zip(_, [], []) :- !.
zip([X|Xs], [Y|Ys], [[X,Y]|Ts]) :- zip(Xs, Ys, Ts).

% END - QUIZ

%range(N, N, [N]).
%range(M, N, L) :- K is M + 1, range(K, N, K), append(L, [M], K).

%twice(X, [H|T]) :- twiceHelepr(X, [H|T], [], [H|T]). % twice(X, T), appenList(

twice(X, L) :- twiceHelper(X, L, [], L, 0).

twiceHelper(X, [X|T], Acc, Orig, 1).
twiceHelper(X, [H|T], [H|T2], Orig, 1) :- twiceHelper(X, T, T2, Orig, 1).
twiceHelper(X, [X|T], [X|T2], Orig, 0) :- twiceHelper(X, T, T2, Orig, 1),
                                            append([X|T2], T, Orig).
twiceHelper(X, [H|T], [H|T2], Orig, 0) :- twiceHelper(X, T, T2, Orig, 0), 
                                            append([H|T2], T, Orig).




%twiceHelper(X, [X|T], [], Orig).
%twiceHelper(X, [H|T], [H|T2], Orig) :- twiceHelper(X, T, T2, Orig).
%twiceHelper(X, [X|T], Acc, Orig) :- twiceHelper(X, T, [], Orig),
%                                   append([X|Acc], T, Orig).
                                   
%twiceHelper(X, [X|T], 



% Determine if a given number is Prime
isPrime(N) :- integer(N), findNumFactors(N, Out), Out = 2.
notPrime(N) :- integer(N), findNumFactors(N, Out), Out =\= 2.

% Count the number of factors of a given number N
findNumFactors(N, Out) :- findNumFactorsHelper(N, 0, N, Out).

findNumFactorsHelper(_, A, 0, A) :- !.
findNumFactorsHelper(N, A, C, R) :- 0 is N mod C, NC is C - 1, NA is A + 1, findNumFactorsHelper(N, NA, NC, R).
findNumFactorsHelper(N, A, C, R) :- X is N mod C, X > 0, NC is C - 1, findNumFactorsHelper(N, A, NC, R).

% Determine if a given value F is a factor of M
isFactor(M, F) :- 0 is M mod F.

% Determine the GCD (G) of given numbers M and N
gcd(M, N, G) :- M >= N, gcdHelper(M, N, G).
gcd(M, N, G) :- M < N, gcdHelper(N, M, G).

gcdHelper(M, N, N) :- 0 is M mod N.
gcdHelper(M, N, G) :- Rem is M mod N, Rem > 0, gcd(N, Rem, G).

% Determine if given numbers M and N are coprime (only positive shared factor is 1)
coPrime(M, N) :- gcd(M, N, G), G is 1.

% Remove all non prime numbers from the given list [H|T]
removeNonPrimes([], []).
removeNonPrimes([H|T], [H|T2]) :- isPrime(H), removeNonPrimes(T, T2).
removeNonPrimes([H|T], T2) :- notPrime(H), removeNonPrimes(T, T2).

% Find all primes that are less than or equal to the given number
primeList(N, L) :- primeListHelper(N, L, 0).

% Commented out because the below gives result in descending order
%primeListHelper(0, []) :- !.
%primeListHelper(N, [N|T]) :- isPrime(N), M is N - 1, primeListHelper(M, T), !.
%primeListHelper(N, L) :- M is N - 1, primeListHelper(M, L), !.

% This provides the result in an ascending order. And is inclusive of initial number.
primeListHelper(N, [N], N) :- isPrime(N), !.
primeListHelper(N, [], N) :- !.
primeListHelper(N, [M|T], M) :- isPrime(M), K is M + 1, primeListHelper(N, T, K), !.
primeListHelper(N, L, M) :- K is M + 1, primeListHelper(N, L, K), !.

% NOTE: This isn't prime factors, this is factors that are prime
% but we want a single factor consiting of primes.... 48 = 2*2*2*2*3 (the multiple 2s are not shown in result)
factorsThatArePrime(M, L) :- findFactors(M, F), removeNonPrimes(F, L).

% Find all the factors of the given number M and give result as list L
findFactors(M, L) :- findFactorsHelper(M, L, M).

findFactorsHelper(_, [], 0) :- !.
findFactorsHelper(M, [C|Result], C) :- 0 is M mod C, NC is C - 1, findFactorsHelper(M, Result, NC).
findFactorsHelper(M, Result, C) :- X is M mod C, X > 0, NC is C - 1, findFactorsHelper(M, Result, NC).

% Rotate a list L to the right N spaces
rotRight(L, 0, L) :- !.
rotRight(L, 1, [H2|T2]) :- last(L, H2), all_but_last(L, T2), !.
rotRight(L, N, [H2|T2]) :- M is N - 1, rotRight(L, M, R), append(T2, [H2], R).

% Rotate a list to the left N spaces (This version is for non-pure numbers N
rotLeft(L, 0, L) :- !.
rotLeft([H1|T1], 1, L) :- last(L, H1), all_but_last(L, T1), !.
rotLeft(L1, N, L2) :- M is N - 1, rotLeft(L1, M, R), last(L2, E), 
                      all_but_last(L2, A), append([E], A, R), !.











% One
del(X, [X|T], T) :- !.
del(X, [H|T], [H|T2]) :- del(X, T, T2).


% REVISIT
perm([], []).
perm([H|T], L) :- del(H, L, R), perm(T, R).

% Three
range(N, N, [N]) :- !.
range(M, N, [M|T]) :- K is M + 1, range(K, N, T).

% USE CLASS EXAMPLE
%appenList(L, [], L) :- !.
%appenList([X|Xs], [Y|Ys], [X|T]) :- appen(Xs, Y, New), appenList(New, Ys, T), !.







































